import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import Landing from './Components/landing';

class App extends Component {
  render() {
    return (
      <div className="App">

        <Landing />
      </div>
    );
  }
}

export default App;
