import React from 'react'
import TicTacToe from './tictactoe'
import { mapNumber } from './utils'

export default class Landing extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      showModal: true,
      playerChar: null,
      computerChar: null,
      startGame: true,
      currentPlayer: '',
      playerCells: [],
      computerCells: [],
      items: [
        'first',
        'second',
        'third',
        'fourth',
        'fifth',
        'sixth',
        'seventh',
        'eighth',
        'nineth'
      ] // use range here
    }
    this.setPlayerChar = this.setPlayerChar.bind(this)
    this.makeComputerMove = this.makeComputerMove.bind(this)
    this.setGame = this.setGame.bind(this)
  }

  setPlayerChar(e) {
    const playerChar = e.target.id
    if (!playerChar) return
    let computerChar
    if (playerChar === 'x') computerChar = 'o'
    else if (playerChar === 'o') computerChar = 'x'
    this.setState(
      {
        playerChar,
        computerChar,
        currentPlayer: 'user',
        showModal: false,
        startGame: true
      },
      () => {
        this.makeComputerMove()
      }
    )
    //<span class="fa fa-times"></span>
  }

  generateRandomNumber() {
    let { items } = this.state
    return items[Math.floor(Math.random() * items.length)]
  }

  setGame(e) {
    const playedCell = e.target.id
    const { items } = this.state
    if (!items.includes(playedCell)) return
    else {
      let isWinner
      let el = document.querySelector(`#${playedCell}`)
      const newArray = items.filter(e => e !== playedCell)
      el.innerHTML =
        this.state.playerChar === 'x'
          ? '<span class="fa fa-times"></span>'
          : '<span class="fa fa-circle-o"></span>'
      const newPlayerCells = [...this.state.playerCells, playedCell]
      this.setState({ items: newArray, playerCells: newPlayerCells, currentPlayer: 'computer', }, () => {
        isWinner = this.checkForWin()
      })
      if (!isWinner ) setTimeout(this.makeComputerMove, 1000)
    }
  }
  makeComputerMove() {
    // let /
    // make random selection from available cells.
    //if currrenyUser == computer disable mouse events.
    const { items } = this.state
    const random = this.generateRandomNumber(items)
    let el = document.querySelector(`#${random}`) //mapNumber
    // // locked the cell to be unclickable
    const newArray = this.state.items.filter(e => e !== random)
    const newComputerCells = [...this.state.computerCells, random]
    // console.log(newArray, 'newArray')
    el.innerHTML =
      this.state.computerChar === 'x'
        ? '<span class="fa fa-times"></span>'
        : '<span class="fa fa-circle-o"></span>'
    this.setState({
      items: newArray,
      currentPlayer: 'user',
      computerCells: newComputerCells
    }, () => {
      // console.log(this.state)
      this.checkForWin()
    })
  }
  checkForWin() {
    
    const { currentPlayer, playerCells, computerCells } = this.state
    console.log(playerCells,1, computerCells, 3, currentPlayer)

    let playerTocheck, isWin
    if (currentPlayer !== 'computer') {
      // console.log('ewww')
      playerTocheck = computerCells
    } else  {
      // console.log('tttte')
      playerTocheck = playerCells
    }
    // console.log(playerTocheck, 'playerTocheck')
    if (playerTocheck.length < 3) return // return when numbers is not equal 3
    const winArray = [
      ['first', 'second', 'third'],
      ['fourth', 'fifth', 'sixth'],
      ['seventh', 'eighth', 'nineth'],
      ['first', 'fourth', 'seventh'],
      ['second', 'fifth', 'eighth'],
      ['third', 'sixth', 'nineth'],
      ['first', 'fifth', 'nineth'],
      ['third', 'fifth', 'seventh']
    ]
    for (var i = 0; i <= winArray.length -1; i++) {
      console.log(playerTocheck)
      // winarray[i].every(elem => playerTocheck.indexOf(elem) > -1);
      isWin = winArray[i].every(val => {
        console.log(val, 'value')
        return playerTocheck.indexOf(val) !== -1
      });
      return isWin;
      
    }
    console.log(isWin, 'isWin')
  
  }
  render() {
    const close = () => {
      this.setState({ showModal: false, startGame: false })
    }
    return (
      <div className="container">
        {this.state.showModal && (
          <div className="modal fades" id="myModal" tabIndex="-1" role="dialog">
            <div className="modal-dialog">
              <div className="modal-content">
                <div className="modal-header">
                  <button
                    type="button"
                    className="close"
                    data-dismiss="modal"
                    aria-label="Close"
                    onClick={close}
                  >
                    <span aria-hidden="true">&times;</span>
                  </button>
                  <h4 className="modal-title text-center">Tic Tac Toe Game</h4>
                </div>
                <div className="modal-body">
                  <h3>Welcome to the Tic Tac Toe Game!</h3>
                  <p>It’s time to choose your figure.</p>
                  <p id="test" />
                </div>
                <div className="modal-footer">
                  {/* <button type="button" id="x" className="btn" data-dismiss="modal"> sssss</button> */}
                  <button
                    type="button"
                    id="x"
                    className="btn"
                    data-dismiss="modal"
                    onClick={this.setPlayerChar}
                  >
                    <span className="fa fa-times" />
                  </button>
                  <button
                    type="button"
                    id="o"
                    className="btn"
                    data-dismiss="modal"
                    onClick={this.setPlayerChar}
                  >
                    <span className="fa fa-circle-o" />
                  </button>
                </div>
              </div>
            </div>
          </div>
        )}

        {/* {!this.state.showModal && <TicTacToe />} //add filled with a pointer none */}
        {!this.state.showModal && this.state.startGame && (
          <div className="game">
            <div id="first" onClick={this.setGame} className="game-field" />
            <div id="second" onClick={this.setGame} className="game-field" />
            <div id="third" onClick={this.setGame} className="game-field" />
            <div id="fourth" onClick={this.setGame} className="game-field" />
            <div id="fifth" onClick={this.setGame} className="game-field" />
            <div id="sixth" onClick={this.setGame} className="game-field" />
            <div id="seventh" onClick={this.setGame} className="game-field" />
            <div id="eighth" onClick={this.setGame} className="game-field" />
            <div id="nineth" onClick={this.setGame} className="game-field" />
          </div>
        )}
      </div>
    )
  }
}
